package jp.alhinc.mori_hiroko.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Map;
import java.util.TreeMap;

public class CalculateSales {

	public static void main(String[] args) {
        Map<String,String> names = new TreeMap<>();
        Map<String,Long> totals = new TreeMap<>();

		BufferedReader br = null;
		try {
			File file = new File(args[0], "branch.lst");
			if(!file.exists()){
				System.out.println("支店定義ファイルが存在しません。");
				return;
            }
            try {
                br = new BufferedReader(new FileReader(file));
                String line;
                while((line = br.readLine()) != null){
                    String[] items = line.split(",");
                    if(items.length != 2 || items[0].matches("^[0-9]{3}") == false){
                        System.out.println("支店定義ファイルのフォーマットが不正です");
                        return;
                    }

                    names.put(items[0], items[1]);
                    totals.put(items[0], 0L);
                }
            } finally {
                br.close();
            }

			FilenameFilter filter = new FilenameFilter(){
				public boolean accept(File file, String str){
					return new File(file, str).isFile() && str.matches("[0-9]{8}.rcd");
				}
            };

            File[] rcdFiles = new File(args[0]).listFiles(filter);
            Arrays.sort(rcdFiles);

			if(rcdFiles != null) {
                int min = Integer.parseInt(rcdFiles[0].getName().substring(0, 8));
                int max = Integer.parseInt(rcdFiles[rcdFiles.length - 1].getName().substring(0, 8));

                if (min + rcdFiles.length - 1 != max) {
                    System.out.println("売上ファイル名が連番になっていません");
                    return;
                }

				BufferedReader br2 = null;
				try {
                    for(File rcdFile : rcdFiles) {
                        br2 = new BufferedReader(new FileReader(rcdFile));
                        String code = br2.readLine();
                        String sales = br2.readLine();
                        if(br2.readLine() != null){
                            System.out.println(rcdFile.getName() + "のフォーマットが不正です");
                            return;
                        }

                        if(names.containsKey(code) == false){
                            System.out.println(rcdFile.getName() + "の支店コードが不正です");
                            return;
                        }

                        Long total = totals.get(code) + Long.parseLong(sales);
                        if(total.toString().length() > 10){
                            System.out.println("合計金額が10桁を超えました");
                            return;
                        }

                        totals.put(code, total);
					}
				}
				finally{
					br2.close();
				}
            }

            PrintWriter pw = new PrintWriter(new BufferedWriter(new FileWriter(new File(args[0], "branch.out"))));

            for (String code : names.keySet()) {
                pw.println(code + "," + names.get(code) + "," + totals.get(code));
            }
			pw.close();
		}
		catch(Exception e){
			System.out.println("予期せぬエラーが発生しました");
		}
	}
}

